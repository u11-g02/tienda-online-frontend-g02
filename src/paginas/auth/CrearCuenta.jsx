import React, { useState, useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from "../../helpers/mensajes.js";

const CrearCuenta = () => {

    //defininir el estado inicial de los elemenmtos input
    const [usuario, setUsuario] = useState({
        idrol: '6366a6607de48c7d8feeb62a',
        nombres: '',
        apellidos: '',
        celular: '',
        correo: '',
        nombreusuario: '',
        clave: '',
        estado: 1
    });

    const { nombres, apellidos, celular, correo, nombreusuario, clave } = usuario;

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        document.getElementById('nombres').focus();
    }, [])

    const crearCuenta = async () => {
        const body = {
            idRol: usuario.idrol,
            nombresUsuario: usuario.nombres,
            apellidosUsuario: usuario.apellidos,
            celularUsuario: usuario.celular,
            correoUsuario: usuario.correo,
            usuarioAcceso: usuario.nombreusuario,
            claveAcceso: usuario.clave,
            estadoUsuario: usuario.estado
        }
        const response = await APIInvoke.invokePOST('/api/usuarios/crear-cuenta', body);
        if (response.ok == "SI") {
            mensajeConfirmacion('success', response.msg);

            setUsuario({
                nombres: '',
                apellidos: '',
                celular: '',
                correo: '',
                nombreusuario: '',
                clave: ''
            });
        } else {
            mensajeConfirmacion('error', response.msg);
        }

    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearCuenta();
    }

    return (
        <div className="hold-transition login-page">
            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <a href="../../index2.html" className="h1"><b>Crear</b> Cuenta</a>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Ingrese los datos del usuario</p>
                        <form onSubmit={onSubmit}>

                            <div className="input-group mb-3">
                                <input type="text" className="form-control"
                                    placeholder="Ingrese sus nombres"
                                    id="nombres"
                                    name="nombres"
                                    value={nombres}
                                    onChange={onChange}
                                    required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input type="text" className="form-control"
                                    placeholder="Ingrese sus apellidos"
                                    id="apellidos"
                                    name="apellidos"
                                    value={apellidos}
                                    onChange={onChange}
                                    required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input type="text" className="form-control"
                                    placeholder="Ingrese su numero de celular"
                                    id="celular"
                                    name="celular"
                                    value={celular}
                                    onChange={onChange}
                                    required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-phone" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input type="email" className="form-control"
                                    placeholder="Ingrese su email"
                                    id="correo"
                                    name="correo"
                                    value={correo}
                                    onChange={onChange}
                                    required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input type="email" className="form-control"
                                    placeholder="Ingrese su usuario de acceso"
                                    id="nombreusuario"
                                    name="nombreusuario"
                                    value={nombreusuario}
                                    onChange={onChange}
                                    required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input type="password" className="form-control"
                                    placeholder="Ingrese su contraseña"
                                    id="clave"
                                    name="clave"
                                    value={clave}
                                    onChange={onChange}
                                    required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">Crear Cuenta</button>
                                </div>
                                <div className="col-12 mt-2">
                                    <Link to={"/"} className="btn btn-info btn-block">Regresar</Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CrearCuenta;