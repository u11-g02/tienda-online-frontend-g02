import React, { useState, useEffect } from "react"
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from "../../helpers/APIInvoke.js";
import mensajeConfirmacion from '../../helpers/mensajes.js';

const Login = () => {

    //poder redireccionar a un componente tipo pagina
    const navigate = useNavigate();

    const [usuario, setUsuario] = useState({
        usu: '',
        cla: ''
    });

    const {usu, cla} = usuario;

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        document.getElementById('usu').focus();
    }, [])

    const login = async () => {
        const body = {
            usuarioAcceso: usuario.usu,
            claveAcceso: usuario.cla
        }

        const response = await APIInvoke.invokePOST(`/api/usuarios/login`, body);

        if (response.ok === "NO_EXISTE") {
            mensajeConfirmacion('error', response.msg);
        }  else if (response.ok === "CLAVE_ERRONEA") {
            mensajeConfirmacion('error', response.msg);
        } else {
            //eliminar el token actual
            localStorage.removeItem("token");
            localStorage.removeItem("username");
            //obtener el token
            const token = response.tokenJwt;

            //guardar el token en el localStore de html5
            localStorage.setItem("token", token);
            localStorage.setItem("username", response.nombresUsuario);

            //redireccionamos el componente dashboard
            navigate("/menu-principal");
        }

        console.log(response);

      
    }

        const onSubmit = (e) => {
            e.preventDefault();
            login();
        }

    return (
        <div className="hold-transition login-page">
            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <a href="../../index2.html" className="h1"><b>Iniciar</b> Sesión</a>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Bienvenido, ingrese sus credenciales.</p>
                        <form onSubmit={onSubmit}>

                            <div className="input-group mb-3">
                                <input type="email" className="form-control" 
                                    placeholder="Email" 
                                    id="usu" 
                                    name="usu"
                                    value={usu} 
                                    onChange={onChange}
                                    required 
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" className="form-control" 
                                    placeholder="Contraseña" 
                                    id="cla" 
                                    name="cla" 
                                    value={cla} 
                                    onChange={onChange}
                                    required />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">Ingresar</button>
                                </div>
                                <div className="col-12 mt-2">
                                    <Link to={"/crear-cuenta"} className="btn btn-success btn-block">Crear Cuenta</Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Login;