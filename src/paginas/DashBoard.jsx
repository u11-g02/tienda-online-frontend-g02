import React from 'react';
import NAvbar from '../componentes/Navbar';
import SidebarContainer from '../componentes/SidebarContainer';
import ContentHeader from '../componentes/ContentHeader';
import Footer from '../componentes/Footer';
import { Link } from 'react-router-dom';

const DashBoard = () => {
    return (
        <div className="wrapper">
            <NAvbar></NAvbar>
            <SidebarContainer></SidebarContainer>

            <div class="content-wrapper">
                <ContentHeader
                       Titulo={"DashBoard"}
                       breadCrumb1={"DashBoard"}
                       breadCrumb2={""}
                       ruta1={"menu-principal"}
                />

                <section className="content">
                    <div className= "container-fluid"> 
                         <div className="row">
                            
                         <div className="col-lg-3 col-6">
                                <div className="small-box bg-info">
                                    <div className="inner">
                                        <h3>Roles</h3>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div className="icon">
                                        <i className="fas fa-user" />
                                    </div>
                                    <Link to={"/roles-admin"} className="small-box-footer"> Listado Roles <i className="fas fa-arrow-circle-right" /></Link>
                                </div>
                            </div>

                            <div className="col-lg-3 col-6">
                                <div className="small-box bg-success">
                                    <div className="inner">
                                        <h3>Ciudades</h3>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div className="icon">
                                        <i className="fas fa-map" />
                                    </div>
                                    <Link to={"/ciudades-admin"} className="small-box-footer">Listado Ciudades <i className="fas fa-arrow-circle-right" /></Link>
                                </div>
                            </div>

                            <div className="col-lg-3 col-6">
                                <div className="small-box bg-warning">
                                    <div className="inner">
                                        <h3>Usuarios</h3>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div className="icon">
                                        <i className="fas fa-users" />
                                    </div>
                                    <Link to={"/usuarios-admin"} className="small-box-footer">Listado Usuarios <i className="fas fa-arrow-circle-right" /></Link>
                                </div>
                            </div>

                            <div className="col-lg-3 col-6">
                                <div className="small-box bg-danger">
                                    <div className="inner">
                                        <h3>&nbsp;</h3>
                                        <p>""</p>
                                    </div>
                                    <div className="icon">
                                        <i className="ion ion-pie-graph" />
                                    </div>
                                    <a href="#" className="small-box-footer">More info <i className="fas fa-arrow-circle-right" /></a>
                                </div>
                            </div>

                         </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>

    );
}

export default DashBoard;