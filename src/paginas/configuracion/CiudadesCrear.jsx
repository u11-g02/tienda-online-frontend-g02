import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import APIInvoke from '../../helpers/APIInvoke.js';
import { useNavigate } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes';

const CiudadesCrear = () => {

    const navigate = useNavigate();

    const [ciudad, setCiudad] = useState({
        nombre: '',
        estado: 1
    });

    const { nombre } = ciudad;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, []);

    const onChange = (e) => {
        setCiudad({
            ...ciudad,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crear();
    }

    const crear = async () => {
        const body = {
            nombreCiudad: ciudad.nombre,
            estadoCiudad: ciudad.estado
        }
        const response = await APIInvoke.invokePOST(`/api/ciudades`, body);
        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            setCiudad({
                nombre: ''
            });
            navigate("/ciudades-admin");
        } else {
            mensajeConfirmacion('error', response.msg);
            setCiudad({
                nombre: '',
                estado: 1
            });
        }
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <div className="content-wrapper">
                <ContentHeader
                    Titulo={"Ciudades"}
                    breadCrumb1={"Listado Ciudades"}
                    breadCrumb2={"Crear Ciudades"}
                    ruta1={"/roles-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Crear Ciudad</h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>

                        <form onSubmit={onSubmit}>
                            <div className="card-body">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Nombre</label>
                                    <input type="text" className="form-control"
                                        id="nombre"
                                        name="nombre"
                                        placeholder="Ingrese el nombre de la ciudad"
                                        value={nombre}
                                        onChange={onChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="card-footer">
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </form>

                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default CiudadesCrear;