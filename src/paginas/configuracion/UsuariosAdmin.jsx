import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import APIInvoke from '../../helpers/APIInvoke.js';
import { Link } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes.js';

const UsuariosAdmin = () => {

    const [arreglo, setArreglo] = useState([]);

    const listadoDocumentos = async () => {
        const response = await APIInvoke.invokeGET(`/api/usuarios`);
        //console.log(response);
        setArreglo(response);
    }

    useEffect(() => {
        listadoDocumentos();
    }, []);

    const borrar = async (e, id) => {
        e.preventDefault();

        const response = await APIInvoke.invokeDELETE(`/api/ciudades/${id}`);

        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            listadoDocumentos();
        } else {
            mensajeConfirmacion('error', response.msg);
        }
    }


    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <div className="content-wrapper">
                <ContentHeader
                    Titulo={"Usuarios"}
                    breadCrumb1={"Listado Usuarios"}
                    breadCrumb2={"Configuración"}
                    ruta1={"/usuarios-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Listado De Usuarios</h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className='row mb-2'>
                                <div className='col-lg-12'>
                                    <Link to={"/ciudades-crear"} className="btn btn-primary">Crear</Link>
                                </div>
                            </div>
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: '10%', textAlign: 'center' }}>&nbsp;</th>
                                        <th style={{ width: '15%', textAlign: 'center' }}>Id</th>
                                        <th style={{ width: '15%', textAlign: 'center' }}>Rol</th>
                                        <th style={{ width: '15%', textAlign: 'center' }}>Ciudad</th>
                                        <th style={{ width: '20%', textAlign: 'center' }}>Nombre</th>
                                        <th style={{ width: '10%', textAlign: 'center' }}>Celular</th>
                                        <th style={{ width: '10%', textAlign: 'center' }}>Usuario</th>
                                        <th style={{ width: '5%', textAlign: 'center' }}>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        arreglo.map(
                                            elemento =>
                                                <tr key={elemento._id}>
                                                    <td style={{ textAlign: 'center' }}>
                                                        <div className="btn-group">
                                                            <button type="button" className="btn btn-primary btn-sm">Opciones</button>
                                                            <button type="button" className="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <span className="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <div className="dropdown-menu" role="menu">
                                                                <Link className="dropdown-item" to={`/ciudades-editar/${elemento._id}`}>Editar</Link>
                                                                <div className="dropdown-divider" />
                                                                <span className="dropdown-item" onClick={(e) => borrar(e, elemento._id)} style={{ cursor: 'pointer' }}>Borrar</span>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td style={{ textAlign: 'center' }}>{elemento._id}</td>
                                                    <td style={{ textAlign: 'center' }}>{elemento.idRol.nombreRol}</td>
                                                    <td style={{ textAlign: 'center' }}>{elemento.idCiudad.nombreCiudad}</td>
                                                    <td style={{ textAlign: 'center' }}>{elemento.nombresUsuario} {elemento.apellidosUsuario}</td>
                                                    <td>{elemento.celularUsuario}</td>
                                                    <td>{elemento.usuarioAcceso}</td>
                                                    <td style={{ textAlign: 'center' }}>
                                                        {elemento.estadoUsuario === 1 ? <span className="text-success">Activo</span> : <span className="text-danger">Inactivo</span>}
                                                    </td>

                                                </tr>
                                        )
                                    }

                                </tbody>
                            </table>


                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default UsuariosAdmin;