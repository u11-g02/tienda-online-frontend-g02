import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import APIInvoke from '../../helpers/APIInvoke.js';
import { useNavigate } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes';

const RolesCrear = () => {

    const navigate = useNavigate();

    const [rol, setRol] = useState({
        nombre: '',
        estado: 1
    });

    const { nombre } = rol;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, []);

    const onChange = (e) => {
        setRol({
            ...rol,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearRol();
    }

    const crearRol = async () => {
        const body = {
            nombreRol: rol.nombre,
            estadoRol: rol.estado
        }
        const response = await APIInvoke.invokePOST(`/api/roles`, body);
        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            setRol({
                nombre: ''
            });
            navigate("/roles-admin");
        } else {
            mensajeConfirmacion('error', response.msg);
            setRol({
                nombre: ''
            });
        }
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <div className="content-wrapper">
                <ContentHeader
                    Titulo={"Roles"}
                    breadCrumb1={"Listado Roles"}
                    breadCrumb2={"Crear Roles"}
                    ruta1={"/roles-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Crear Rol</h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>

                        <form onSubmit={onSubmit}>
                            <div className="card-body">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Nombre</label>
                                    <input type="text" className="form-control" 
                                        id="nombre" 
                                        name="nombre" 
                                        placeholder="Ingrese el nombre del rol"
                                        value={nombre}
                                        onChange={onChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="card-footer">
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </form>

                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default RolesCrear;