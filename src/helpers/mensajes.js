import Swal from 'sweetalert2';

const mensajeConfirmacion = (icon, msg, titulo)  => {
    Swal.fire({
        icon: icon,
        title: titulo,
        text: msg,
    })
}

export default mensajeConfirmacion