import React from 'react';
import { Link } from 'react-router-dom';
import Menu from './Menu';

const SidebarContainer = () => {
    return (
        <aside className="main-sidebar sidebar-dark-primary elevation-4">
            <Link to={"#"} className="brand-link">
                <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
                <span className="brand-text font-weight-light">MiDulceOnline</span>
            </Link>
            <div className="sidebar">
                {/* Sidebar user (optional) */}
                <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div className="image">
                        <img src="../../dist/img/user2-160x160.jpg" className="img-circle elevation-2" alt="User Image" />
                    </div>
                    <div className="info">
                        <Link to={"/menu-principal"} className="d-block">{localStorage.getItem("username")}</Link>
                    </div>
                </div>

                 <Menu></Menu>

            </div>
        </aside>

    );
}

export default SidebarContainer;