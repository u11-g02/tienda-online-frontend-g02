import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import mensajeConfirmacion from '../helpers/mensajes';

const NAvbar = () => {

    const navigate = useNavigate();

    const cerrarSesion = () => {
        //eliminar todo lo del localstore
        localStorage.removeItem("token");
        localStorage.removeItem("username");

        mensajeConfirmacion('success', 'Sesión finalizada correctamente.');
        //redireccionar al login
        navigate("/");

    }
    return (
        <nav className="main-header navbar navbar-expand navbar-white navbar-light">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" data-widget="pushmenu" to={"#"} role="button"><i className="fas fa-bars" /></Link>
                </li>
                <li className="nav-item d-none d-sm-inline-block">
                    <strong className="nav-link" onClick={cerrarSesion} style={{ cursor: 'pointer' }}>Salir</strong>
                </li>
            </ul>
            <ul className="navbar-nav ml-auto">

                <li className="nav-item">
                    <Link className="nav-link" data-widget="fullscreen" to={"#"} role="button">
                        <i className="fas fa-expand-arrows-alt" />
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" data-widget="control-sidebar" data-slide="true" to={"#"} role="button">
                        <i className="fas fa-th-large" />
                    </Link>
                </li>
            </ul>
        </nav>

    );
}

export default NAvbar;