import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => {
    return (
        <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li className="nav-item">
                    <Link to={"/menu-principal"} className="nav-link">
                        <i className="nav-icon fas fa-th" />
                        <p>
                            DashBoard
                        </p>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"#"} className="nav-link">
                        <i className="nav-icon fas fa-cog" />
                        <p>
                            Configuración
                            <i className="fas fa-angle-left right" />
                        </p>
                    </Link>
                    <ul className="nav nav-treeview">
                        <li className="nav-item">
                            <Link to={"/roles-admin"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Roles</p>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/ciudades-admin"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Ciudades</p>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/usuarios-admin"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Usuarios</p>
                            </Link>
                        </li>
                    </ul>
                </li>

                <li className="nav-item">
                    <Link to={"#"} className="nav-link">
                        <i className="nav-icon fas fa-barcode" />
                        <p>
                            Inventario
                            <i className="fas fa-angle-left right" />
                        </p>
                    </Link>
                    <ul className="nav nav-treeview">
                        <li className="nav-item">
                            <Link to={"#"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Categorias</p>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"#"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Productos</p>
                            </Link>
                        </li>
                    </ul>
                </li>

                <li className="nav-item">
                    <Link to={"#"} className="nav-link">
                        <i className="nav-icon fas fa-shopping-cart" />
                        <p>
                            Pedidos
                            <i className="fas fa-angle-left right" />
                        </p>
                    </Link>
                    <ul className="nav nav-treeview">
                        <li className="nav-item">
                            <Link to={"#"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Listado Pedidos</p>
                            </Link>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

    );
}

export default Menu;