import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import CrearCuenta from './paginas/auth/CrearCuenta';
import Login from './paginas/auth/Login';
import DashBoard from './paginas/DashBoard';
import RolesAdmin from './paginas/configuracion/RolesAdmin';
import RolesCrear from './paginas/configuracion/RolesCrear';
import RolesEditar from './paginas/configuracion/RolesEditar';
import CiudadesAdmin from './paginas/configuracion/CiudadesAdmin';
import CiudadesCrear from './paginas/configuracion/CiudadesCrear';
import CiudadesEditar from './paginas/configuracion/CiudadesEditar';
import UsuariosAdmin from './paginas/configuracion/UsuariosAdmin';

function App() {
  return (
   <Fragment>
     <Router>
      <Routes>
        <Route path='/' exact element={<Login/>} />
        <Route path='/crear-cuenta' exact element={<CrearCuenta/>} />
        <Route path='/menu-principal' exact element={<DashBoard/>} />
        <Route path='/roles-admin' exact element={<RolesAdmin/>} />
        <Route path='/roles-crear' exact element={<RolesCrear/>} />
        <Route path='/roles-editar/:id' exact element={<RolesEditar/>} />
        <Route path='/ciudades-admin' exact element={<CiudadesAdmin/>} />
        <Route path='/ciudades-crear' exact element={<CiudadesCrear/>} />
        <Route path='/ciudades-editar/:id' exact element={<CiudadesEditar/>} />
        <Route path='/usuarios-admin' exact element={<UsuariosAdmin/>} />
      </Routes>
     </Router>
   </Fragment>
  );
}

export default App;
